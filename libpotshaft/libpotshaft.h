/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef LIBPOTSHAFT_H
#define LIBPOTSHAFT_H

#include <webkit2/webkit2.h>

G_BEGIN_DECLS

const gchar *potshaft_get_extension_path (void);

void potshaft_init (void);
void potshaft_init_for_context (WebKitWebContext *context);

G_END_DECLS

#endif /* LIBPOTSHAFT_H */
