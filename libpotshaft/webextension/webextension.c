/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <seed.h>
#include <webkit2/webkit-web-extension.h>

static SeedEngine *eng = NULL;

static void
window_object_cleared_cb (WebKitScriptWorld *world,
                          WebKitWebPage *web_page,
                          WebKitFrame *frame,
                          gpointer data)
{
  JSGlobalContextRef context;
  context = webkit_frame_get_javascript_context_for_script_world (frame, world);
  if (!eng)
    {
      eng = seed_init_with_context (NULL, NULL, context);
    }
  else
    {
      seed_prepare_global_context (context);
    }
}

G_MODULE_EXPORT void
webkit_web_extension_initialize (WebKitWebExtension *extension)
{
  g_signal_connect (webkit_script_world_get_default (),
                    "window-object-cleared",
                    G_CALLBACK (window_object_cleared_cb),
                    extension);
}
