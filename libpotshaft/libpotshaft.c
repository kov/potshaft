/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "libpotshaft.h"

static void
initialize_web_extensions (WebKitWebContext *context,
                           gpointer user_data)
{
  const char *path = potshaft_get_extension_path ();

  webkit_web_context_set_web_extensions_directory (context, path);
}

/**
 * potshaft_get_extension_path:
 *
 * Return the default potshaft's extension installation dir or the
 * value of the env variable "POTSHAFT_EXTENSION_PATH", when set.
 *
 * Return value: The potshaft's extension path.
 */
const gchar *
potshaft_get_extension_path (void)
{
  const char *path = g_getenv ("POTSHAFT_EXTENSION_PATH");
  if (!path)
    path = PREFIX "/lib/potshaft-1.0/web-extensions/";
  return path;
}

/**
 * potshaft_init_for_context:
 * @context: A #WebKitWebContext object.
 *
 * Convenience method to configure the WebKit web context to load
 * web extensions from the potshaft's extension path
 * (see potshaft_get_extension_path()).
 *
 * Note that this convenience method will override the web context's
 * currently configured web extensions search dir by using
 * @webkit_web_context_set_web_extensions_directory().
 */
void
potshaft_init_for_context (WebKitWebContext *context)
{
  g_signal_connect (context,
                    "initialize-web-extensions",
                    G_CALLBACK (initialize_web_extensions),
                    NULL);
}

/**
 * potshaft_init:
 *
 * Same as potshaft_init_for_context() with webkit_web_context_get_default()
 * as @context.
 */
void
potshaft_init (void)
{
  potshaft_init_for_context (webkit_web_context_get_default ());
}
