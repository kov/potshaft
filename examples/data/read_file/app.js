const Gio = imports.gi.Gio;

var f = Gio.file_new_for_path('/etc/resolv.conf');
f.load_contents_async(null, function(f, res) {
  let contents;
  let output = document.getElementById('output');
  try {
      contents = f.load_contents_finish(res)[1];
  } catch (e) {
      output.innerText = '*** ERROR: ' + e.message;
      return;
  }
  output.innerText = contents;
});
