function updateChart() {
   var result = Seed.spawn("/bin/sh -c \"top -bn 2 -d 1 | grep '^%Cpu' | tail -n 1 | gawk '{print $2+$4+$6}' \"");
   var integer = parseInt(result.stdout);
   print("CPU Usage: " + integer);
   myChart.data.datasets[0].data[0] = integer
   myChart.update();
}

setInterval(updateChart, 2000);
