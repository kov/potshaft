/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "libpotshaft.h"
#include <webkit2/webkit2.h>

static gboolean
web_view_ready_to_show_cb (WebKitWebView *web_view)
{
  GtkWidget *window = gtk_widget_get_toplevel (GTK_WIDGET (web_view));
  gtk_widget_show_all (window);
  return TRUE;
}

static WebKitWebView *
web_view_create_cb (WebKitWebView *web_view,
                    WebKitNavigationAction *navigation_action,
                    gpointer user_data)
{
  GtkWidget *new_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size (GTK_WINDOW (new_window), 600, 300);

  WebKitWebView *new_web_view = WEBKIT_WEB_VIEW (webkit_web_view_new_with_related_view (web_view));
  g_signal_connect (G_OBJECT (new_web_view), "ready-to-show",
                    G_CALLBACK (web_view_ready_to_show_cb), NULL);

  gtk_container_add (GTK_CONTAINER (new_window), GTK_WIDGET (new_web_view));

  return new_web_view;
}

int main (int argc, char **argv)
{
  GtkWidget *window;
  WebKitWebView *web_view;
  WebKitSettings *web_settings;
  g_autofree char *program_path = g_path_get_dirname (argv[0]);
  g_autofree char *extension_path = g_strdup_printf ("%s/../../libpotshaft/webextension/.libs/", program_path);
  g_autofree char *file_path;
  const char *default_example_name = "read_file";

  if (argc == 2)
    default_example_name = argv[1];
  file_path = g_strdup_printf ("file:///%s/../data/%s/index.html",
                               program_path, default_example_name);

  gtk_init (&argc, &argv);

  g_setenv ("POTSHAFT_EXTENSION_PATH", extension_path, FALSE);
  potshaft_init ();

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
  gtk_window_set_default_size (GTK_WINDOW (window), 800, 600);

  web_view = WEBKIT_WEB_VIEW (webkit_web_view_new ());
  web_settings = webkit_web_view_get_settings (web_view);
  webkit_settings_set_javascript_can_open_windows_automatically (web_settings, TRUE);
  g_signal_connect (web_view, "create", G_CALLBACK (web_view_create_cb), NULL);

  gtk_container_add (GTK_CONTAINER (window), GTK_WIDGET (web_view));

  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
  gtk_window_resize (GTK_WINDOW (window), 800, 600);

  g_print ("Loading %s\n", file_path);
  webkit_web_view_load_uri (web_view, file_path);

  gtk_widget_show_all (window);

  gtk_main ();
  return 0;
}
