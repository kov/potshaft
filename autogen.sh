#! /bin/bash
# Run this to generate all the initial makefiles, etc.

PROJECT=libpotshaft

test -d m4 || mkdir m4
gtkdocize || exit 1
autoreconf -v -f --install -Wno-portability || exit $?
./configure "$@"  && echo "Now type 'make' to compile $PROJECT."


